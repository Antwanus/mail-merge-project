# TODO: Create a letter using starting_letter.txt
# for each name in invited_names.txt
# Replace the [name] placeholder with the actual name.
# Save the letters in the folder "ReadyToSend".

# Hint1: This method will help you: https://www.w3schools.com/python/ref_file_readlines.asp
# Hint2: This method will also help you: https://www.w3schools.com/python/ref_string_replace.asp
# Hint3: THis method will help you: https://www.w3schools.com/python/ref_string_strip.asp

def get_starting_letter_and_replace_name_with(new_name):
    with open("Input/Letters/starting_letter.txt", mode="r") as starting_letter:
        contents = starting_letter.read()
        return contents.replace("[name]", new_name)

def names_file_to_list():
    with open("Input/Names/invited_names.txt", mode='r') as names:
        contents2 = names.read()
        array = contents2.split('\n')
        return array

def write_text_to_file(text, addressee):
    filename = 'letter_for_' + addressee + '.txt'
    with open("Output/ReadyToSend/" + filename, mode='w') as file:
        file.write(text)


list_of_names = names_file_to_list()

for name in list_of_names:
    new_text = get_starting_letter_and_replace_name_with(name)
    write_text_to_file(new_text, name)

